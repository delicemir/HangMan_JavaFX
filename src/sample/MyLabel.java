package sample;


import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class MyLabel extends Label {
    private String tag;
    private String color = "#B8B8B8";

    public MyLabel(){
        setLabel();
    }

    public String getTag() {
        return tag;
    }

    //da bude postavljen tag velikim slovima
    public void setTag(String tag) {
        this.tag = tag.toUpperCase();
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        setStyle("-fx-background-color: " + color);
    }

    public void setLabel() {
        setStyle("-fx-background-color: " + color+";"+
        "-fx-underline: true;");
        setTextFill(Color.BLACK);
        setPrefSize(25, 25);
        setAlignment(Pos.CENTER);
        setFont(Font.font("Arial", FontWeight.BOLD,16));
    }
}
