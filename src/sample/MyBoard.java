package sample;

import javafx.scene.layout.Pane;
import java.util.ArrayList;

public class MyBoard extends Pane{

    private ArrayList<MyLabel> myLabels= new ArrayList<>();;
    private String color;

    private static int i=0;

    public MyBoard(){
        setPrefHeight(60);
        setColor(ColorType.DARKGRAY.color);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        setStyle("-fx-background-color: " + color);
    }

    public ArrayList<MyLabel> getMyLabels() {
        return myLabels;
    }

    public void clearBoard() {
        getChildren().removeAll(myLabels);
        myLabels.clear();
        setPrefWidth(0.0);
    }

    private void addLabels(MyLabel label) {
        myLabels.add(label);
        getChildren().add(label);
    }

    public void createBoard(String word, long timePeriodMiliSec) {

        clearBoard();
        int numOfLetters = word.length();
        double parentWidth = getParent().getLayoutBounds().getWidth();
        double maxChildWidth = 470;
        double maxLettersInRow = Math.floor(maxChildWidth / 45);
        double rowCount = Math.ceil(numOfLetters / maxLettersInRow);

        for (int j = 0; j < rowCount; j++){
            for (int i = 0; i < maxLettersInRow; i++){
                int nextIndex = j * (int)maxLettersInRow + i;
                if (numOfLetters <= nextIndex){
                    break;
                }
                MyLabel label = new MyLabel();
                label.setTag(String.valueOf(word.charAt(nextIndex)));
                label.setLayoutX(20 + i * 45);
                if (j == 0){
                    label.setLayoutY(17.5);
                } else {
                    label.setLayoutY(17.5 + 42.5 * j);
                }
                addLabels(label);      //dodavanje labela na board
            }
        }
        if (rowCount == 1){
            setPrefHeight(60);
            setPrefWidth(numOfLetters *45 + 20);
        } else {
            setPrefHeight(17.5 + rowCount*42.5);
            setPrefWidth(maxChildWidth);
        }

    }



    public boolean findLetter(String letter){
        boolean result = false;
        for (MyLabel label : myLabels){
            if (label.getTag().toUpperCase().equals(letter.toUpperCase())){
                label.setText(label.getTag().toUpperCase());          // da bude ispis velikim štampanim slovima
                result = true;
            }
        }
        return result;
    }

    // metod koji ispituje da li je otkrivena zagonetna riječ
    public boolean isRevealedSecretWord() {

        String tempWord="";  //imat će slova koliko je korisnik pogodio
        String secretWord="";  // tražena riječ nalazi se u tagovima labela

        for (MyLabel label : myLabels){
            tempWord+=label.getText();      //prvo treba formirati riječ od slova iz labela
            secretWord+=label.getTag().toUpperCase();
        }
        if(tempWord.equals(secretWord)){
            return true;
        }
        return false;
    }


}

//TODO: Napraviti da se unosom nove riječi automatski generiše onoliko labela koliko riječ ima slova - URAĐENO
//TODO: napraviti da provjeri da li je otkrivena kompletna riječ URAĐENO

