package sample;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.*;
import sample.Hang.Hang;

import java.util.ArrayList;


public class BodyBoard extends Pane {

    private String color;
    private double wBodyPane;
    private double hBodyPane;
    private Hangman hangman=new Hangman();  //čovjek
    private Hang hang = new Hang();      //vješalo
    private Label infoText = new Label();  // poruka za kraj igre

    private ArrayList<Shape> addedBodyParts = new ArrayList<>();  //lista trenutno nacrtanih dijelova tijela, prati broj pogrešaka

    public BodyBoard() {
        setwBodyPane(200);
        sethBodyPane(320);
        setPrefSize(getwBodyPane(),gethBodyPane());
        setInfoText("YOU ARE LOSER!",150,ColorType.RED);
        setStyle("-fx-border-color: #03C;" +
                "-fx-border-radius: 5px;" +
                "-fx-border-width: 2px;" +
                "-fx-background-color: darkgray;" +
                "-fx-background-radius: 5px;");

    }
    public BodyBoard(Node... children) {
        super(children);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        setStyle("-fx-background-color: " + color);
    }

    public Label getInfoText() {
        return infoText;
    }

    public void setInfoText(String text, double prefW, ColorType color) {
        this.infoText.setText(text);
        this.infoText.setPrefSize(prefW,30);
        this.infoText.setFont(Font.font("Arial", FontWeight.BOLD,14));
        this.infoText.setTextFill(Color.valueOf(color.color));
        this.infoText.setLayoutX(getwBodyPane()/2-infoText.getPrefWidth()/2);
        this.infoText.setLayoutY(gethBodyPane()-infoText.getPrefHeight()-5);
        this.infoText.setAlignment(Pos.CENTER);
    }

    public double getwBodyPane() {
        return wBodyPane;
    }

    public void setwBodyPane(double wBodyPane) {
        this.wBodyPane = wBodyPane;
    }

    public double gethBodyPane() {
        return hBodyPane;
    }

    public void sethBodyPane(double hBodyPane) {
        this.hBodyPane = hBodyPane;
    }

    //nacrtaj tijelo - čovjeka, dodaj na BodyBoardPane
    public void drawHangMan(double w,int fail) throws Exception{

        hangman.setAllBodyParts(w);
        switch (fail) {
            case 1: {   getChildren().add(hangman.getHead()); addedBodyParts.add(hangman.getHead());     break;  }
            case 2: {   getChildren().add(hangman.getBody());  addedBodyParts.add(hangman.getBody());    break;  }
            case 3: {   getChildren().add(hangman.getLeftArm());  addedBodyParts.add(hangman.getLeftArm());  break; }
            case 4: {   getChildren().add(hangman.getRightArm()); addedBodyParts.add(hangman.getRightArm());  break; }
            case 5: {   getChildren().add(hangman.getLeftLeg());  addedBodyParts.add(hangman.getLeftLeg());  break; }
            case 6: {   getChildren().add(hangman.getRightLeg()); addedBodyParts.add(hangman.getRightLeg());
                        addInfoText();
                        throw new Exception("YOU ARE LOSER!");
            }
            default:
        }

    }

    public void addInfoText() {
        this.getChildren().add(infoText);
    }
    public void removeInfoText() {
        this.getChildren().remove(infoText);
    }


    // nacrtaj vješalo unutar BodyBoardPane
    public void drawHang(double w, double h) {
        getChildren().addAll(hang.getHangLines(w,h));     // nacrtaj vješalo
    }

    // očisti sve elemente - i vješalo i tijelo
    public void clearBodyBoard() {
        getChildren().removeAll(hang.getHangLines());
        getChildren().removeAll(addedBodyParts);
        removeInfoText();

        hang.getHangLines().clear();
        hangman.getBodyParts().clear();

    }

    @Override
    public String toString() {
        return "BodyBoard{" +
                "hangman=" + hangman +
                ", hang=" + hang +
                '}';
    }
}

//TODO: Napraviti da baca Exception i da se hvata u Controleru i dalje da program nastavlja sa radom - URAĐENO