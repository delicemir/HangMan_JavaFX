package sample;

import javafx.scene.shape.Shape;
import sample.bodyParts.*;

import java.util.ArrayList;

public class Hangman {

    private Head head;
    private LeftArm leftArm;
    private RightArm rightArm;
    private LeftLeg leftLeg;
    private RightLeg rightLeg;
    private Body body;

    private double w;

    private ArrayList<Shape> bodyParts = new ArrayList<>();

    public Hangman() {
    }

    public ArrayList<Shape> getBodyParts() {
        bodyParts.add(getHead());
        bodyParts.add(getBody());
        bodyParts.add(getLeftArm());
        bodyParts.add(getRightArm());
        bodyParts.add(getLeftLeg());
        bodyParts.add(getRightLeg());

        return bodyParts;
    }

    // podesi širinu da se može centrirati nacrtano tijelo
    public void setAllBodyParts(double w) {
        setW(w); //podesi širinu
        setHead();
        setBody();
        setLeftArm();
        setRightArm();
        setLeftLeg();
        setRightLeg();
    }

    public double getW() {
        return w;
    }

    public void setW(double w) {
        this.w = w;
    }

    public Head getHead() {
        return head;
    }

    public LeftArm getLeftArm() {
        return leftArm;
    }

    public RightArm getRightArm() {
        return rightArm;
    }

    public LeftLeg getLeftLeg() {
        return leftLeg;
    }

    public RightLeg getRightLeg() {
        return rightLeg;
    }

    public Body getBody() {
        return body;
    }


    public void setHead() {
        this.head =new Head(w/2,50,30);
    }

    public void setBody() {
        this.body =  new Body(w/2,150,40,70);
    }

    public void setLeftArm() {
        this.leftArm = new LeftArm(w/2-50,110,10,30);
    }

    public void setRightArm() {
        this.rightArm = new RightArm(w/2+50,110,10,30);
    }

    public void setLeftLeg() {
        this.leftLeg = new LeftLeg(w/2-30,253,15,40);
    }

    public void setRightLeg() {
        this.rightLeg = new RightLeg(w/2+30,253,15,40);
    }



    @Override
    public String toString() {
        return "Hangman{" +
                "head=" + head +
                ", leftArm=" + leftArm +
                ", rightArm=" + rightArm +
                ", leftLeg=" + leftLeg +
                ", rightLeg=" + rightLeg +
                ", body=" + body +
                '}';
    }
}
