package sample.Hang;

import javafx.scene.layout.Pane;
import sample.BodyBoard;

import java.util.ArrayList;

public class Hang {

    //dijelovi vješala - linije
    private HangLine bottomLine;
    private HangLine verticalLine;
    private HangLine topLine;
    private HangLine headLine;

    private double w;
    private double h;
    private double m=5; // margina


    private ArrayList<HangLine> hangLines = new ArrayList<>();  // kompletno vješalo

    public Hang() {

    }

    // vraća dijelove - listu vješala za potrebe brisanja
    public ArrayList<HangLine> getHangLines() {
        return hangLines;
    }

    public ArrayList<HangLine> getHangLines(double w, double h) {

        hangLines.clear();

        this.w=w;
        this.h=h;

        bottomLine= new HangLine(m,h-m,w-m,h-m);
        verticalLine= new HangLine(w-m,h-m,w-m,m);
        topLine= new HangLine(w-m,m,w/2,m);
        headLine= new HangLine(w/2,m,w/2,20-m);

        hangLines.add(bottomLine);
        hangLines.add(verticalLine);
        hangLines.add(topLine);
        hangLines.add(headLine);

        return hangLines;
    }


    @Override
    public String toString() {
        return "Hang{" +
                "bottomLine=" + bottomLine +
                ", verticalLine=" + verticalLine +
                ", topLine=" + topLine +
                ", headLine=" + headLine +
                '}';
    }
}
