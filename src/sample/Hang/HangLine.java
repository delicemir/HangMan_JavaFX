package sample.Hang;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import sample.ColorType;

public class HangLine extends Line {

    private double lineWidth;
    private String color;

    public HangLine() {
    }

    public HangLine(double startX, double startY, double endX, double endY) {
        super(startX, startY, endX, endY);
        setLineWidth(5);
        setColor(ColorType.DARKGREEN.color);
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
        setStrokeWidth(lineWidth);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        setStroke(Color.valueOf(color));
    }
}
