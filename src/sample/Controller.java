package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    public PasswordField word;
    public Button start;
    public TextFieldLimited letter;
    public Button ok;
    public MyBoard myboard;
    public MyLetterPane letters;
    public BodyBoard bodyboard;
    public Pane background;
    public Label labWordInfo;
    public Label labLetterInfo;

    private double w;  //širina okvira za tijelo
    private double h;  //visina okvira za tijelo

    private static int countFailure=0;  //brojač grešaka
    private boolean finishedGame=false;

    //metod koji provjerava da li je uneseno samo jedno slovo
    public boolean testLetter(TextFieldLimited letter) {
        if(!letter.getText().isEmpty() && letter.getText().matches("[A-Za-z]")){
            setLabelInfo(labLetterInfo,"",ColorType.RED);
            return true;
        }
        setLabelInfo(labLetterInfo,"Allowed only one letter A-Z or a-z!",ColorType.RED);
        return false;
    }

    //metod koji provjerava da li su unešena samo slova, max 8 karaktera
    public boolean testWord(TextField word) {
        if(!word.getText().isEmpty() && word.getText().matches("[A-Za-z]{1,30}")){
            return true;
        }
        return false;
    }

    public void initGame() {
        letter.setMaxLength(1);
        w = bodyboard.getPrefWidth();
        h = bodyboard.getPrefHeight();
        background.setStyle("-fx-background-color: #DDD;" +
                "-fx-border-color: #03C;" +
                "-fx-border-width: 1px;");
        start.setText("START");
        setLabelInfo(labLetterInfo,"",ColorType.RED);
        setLabelInfo(labWordInfo,"",ColorType.RED);
    }


    public void setLabelInfo(Label label,String text, ColorType colorType) {
        label.setText(text);
        label.setTextFill(Color.valueOf(colorType.color));
        removeLabelInfo(label);
    }

    public void removeLabelInfo(Label label) {
        if(background.getChildren().contains(label) && label.getText().isEmpty()) {
            background.getChildren().remove(label);
        }
        else if(!background.getChildren().contains(label) && !testWord(word)){
            background.getChildren().add(label);
        }
    }

    public void clearBoards() {
        letters.clearLabels();
        bodyboard.clearBodyBoard();
        myboard.clearBoard();
        setLabelInfo(labLetterInfo,"",ColorType.RED);
    }

    public void onStart() {
        finishedGame=false;
        countFailure=0;
        clearBoards();
        if(testWord(word)) {
            myboard.createBoard(word.getText(), 250);  // ispisuje slova nakog određenog vremena u mili sekundama
            bodyboard.drawHang(w, h);
            setLabelInfo(labWordInfo, "", ColorType.RED);
            setLabelInfo(labLetterInfo, "", ColorType.RED);
            letter.requestFocus();
        }
        else {
            setLabelInfo(labWordInfo,"Allowed only A-Z letters (max. length 30 chars)!",ColorType.RED);
            word.requestFocus();
        }
        word.clear();
        letter.clear();

    }

    public void onLetterInput() {
        if(testLetter(letter) && !finishedGame && !myboard.getMyLabels().isEmpty()) {
            boolean isRevealedLetter = myboard.findLetter(letter.getText());
            boolean isDuplicateLetter = letters.duplicateLetter(letter.getText());

            letters.addLetter(letter.getText(), isRevealedLetter);

            if (isDuplicateLetter) {
                setLabelInfo(labLetterInfo, "You allready tried that letter!", ColorType.RED);
            } else {
                setLabelInfo(labLetterInfo, "", ColorType.RED);
            }

            //ako nije pogođeno - otkriveno slovo i nije duplo slovo broji pogreške
            if (!isRevealedLetter && !isDuplicateLetter) {
                countFailure++;
                try {
                    bodyboard.drawHangMan(w, countFailure);  // crtaj dijelove tijela
                } catch (Exception e) {
                    bodyboard.setInfoText(e.getMessage(), 150, ColorType.RED);
                }
            }

            if (countFailure == 6 || myboard.isRevealedSecretWord()) {
                finishedGame = true;
                start.setText("NEW GAME?");
                if (myboard.isRevealedSecretWord()) {
                    bodyboard.setInfoText("YOU ARE WINNER !!!", 150, ColorType.ROYALBLUE);
                    bodyboard.addInfoText();
                }
            }
            letter.clear();                          // očisti polje za unos nakon što se slovo testira
            if(finishedGame){
                word.requestFocus();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        initGame();

        start.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onStart();
            }
        });
        ok.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onLetterInput();
            }
        });


    }

    public void onLetterEnter(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            onLetterInput();
        }
    }

    public void onWordEnter(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            onStart();
        }
    }


    //TODO: nakon ponovnog klika na START, da se očisti lista slova, jer se generišu nova slova - nova igra počinje - URAĐENO
}

