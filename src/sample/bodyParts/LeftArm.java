package sample.bodyParts;

import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import sample.ColorType;

public class LeftArm extends Ellipse {

    private double lineWidth;
    private String color;

    public LeftArm() {
    }

    public LeftArm(double radiusX, double radiusY) {
        super(radiusX, radiusY);
    }

    public LeftArm(double centerX, double centerY, double radiusX, double radiusY) {
        super(centerX, centerY, radiusX, radiusY);
        setLineWidth(3);
        setColor(ColorType.BLUE.color);
        setFill(Color.valueOf(ColorType.DARKGRAY.color));
        setRotate(55);
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
        setStrokeWidth(lineWidth);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        setStroke(Color.valueOf(color));
    }
}
