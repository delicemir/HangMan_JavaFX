package sample.bodyParts;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import sample.ColorType;

public class Head extends Circle {

    private double lineWidth;
    private String color;


    public Head(double radius) {
        super(radius);

    }

    public Head(double radius, Paint fill) {
        super(radius, fill);
    }

    public Head() {
    }

    public Head(double centerX, double centerY, double radius) {
        super(centerX, centerY, radius);
        setLineWidth(3);
        setFill(Color.valueOf(ColorType.DARKGRAY.color));
        setColor(ColorType.BLUE.color);
    }

    public Head(double centerX, double centerY, double radius, Paint fill) {
        super(centerX, centerY, radius, fill);
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
        setStrokeWidth(lineWidth);

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        setStroke(Color.valueOf(color));
    }
}
