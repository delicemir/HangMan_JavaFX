package sample;

public enum ColorType {
    RED("#FF0000"),
    GREEN("#00FF00"),
    DARKGRAY("#404040"),
    BLUE("#0033CC"),     // nijansa plave
    SILVER("#C0C0C0"),
    DARKGREEN("#006400"),
    ROYALBLUE("4169E1"),
    LIGHTCORAL("F08080");

    public String color;
    ColorType(String s) {
       color = s;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
