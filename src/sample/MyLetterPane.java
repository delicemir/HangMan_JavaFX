package sample;

import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MyLetterPane extends Pane{

    private String color;

    private ArrayList<MyLabel> labels = new ArrayList<>();

    public MyLetterPane(){
        setPrefHeight(45);
        setColor(ColorType.DARKGRAY.color);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        setStyle("-fx-background-color: "+color);
    }

    public ArrayList<MyLabel> getLabels() {
        return labels;
    }

    public void clearLabels() {
        this.getChildren().removeAll(labels);
        getLabels().clear();
        setPrefWidth(0);
    }


    public boolean duplicateLetter(String letter) {  //metod za provjeru da li smo već unijeli slovo, da se spriječi unos duplih slova

        for (MyLabel label: labels) {
            if(label.getText().toUpperCase().equals(letter.toUpperCase())) {
                return true;
            }
        }
        return false;

    }

    public void addLetter(String letter, boolean isValid){
        if(!duplicateLetter(letter)) {                        //ako nije duplo slovo, dozvoli unos
            MyLabel label = new MyLabel();
            label.setText(letter);
            label.setId(String.valueOf(labels.size()));
            if (isValid) {
                label.setColor(ColorType.GREEN.color);
            } else {
                label.setColor(ColorType.RED.color);
            }
            getChildren().removeAll(labels);
            labels.add(label);                                  //dodavanje slova u listu i sortiranje (code prema dole)
            Collections.sort(labels, new Comparator<MyLabel>() {
                @Override
                public int compare(MyLabel o1, MyLabel o2) {
                    if (o1.getText().toCharArray()[0] > o2.getText().toCharArray()[0]) {
                        return 1;
                    }
                    if (o1.getText().toCharArray()[0] < o2.getText().toCharArray()[0]) {
                        return -1;
                    }
                    return 0;
                }
            });

            for (int i = 0; i < labels.size(); i++) {
                labels.get(i).setLayoutX(15 + i * 35);
                labels.get(i).setLayoutY(10);
                getChildren().add(labels.get(i));
            }
            setPrefWidth(labels.size() * 15 + (labels.size() + 1) * 20);
        }

    }

    //TODO: Napraviti da se ne duplaju slova u listi - URAĐENO
}
